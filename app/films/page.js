import Layout from '../../components/Layout';

const FilmsList = ( {films} ) => {
  return (
    <Layout>
      <h1 className='text-5xl md:text-6xl font-extrabold leanding-tighter mb-4'>
        <span className='bg-clip-text text-transparent     
         bg-gradient-to-t
        from-neutral-300
        via-neutral-500
        to-black
         py-2'>
          Films
        </span>
      </h1>


    </Layout>
  )
}

export default FilmsList;


// export async function getStaticProps(){
//   const filmsResponse = await fetcher(`${process.env.NEXT_PUBLIC_STRAPI_URL}/films`)
//   console.log(filmsResponse);
//   return {
//     props: {
//       films: filmsResponse
//     }
//   }
// }