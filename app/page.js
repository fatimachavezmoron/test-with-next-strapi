import Image from 'next/image'
import Layout from '../components/Layout'

export default function Home() {
  return (
    <Layout>
    <main className="
      min-h-screen 
      items-center
      rounded-lg
      p-44        
      bg-gradient-to-t
      from-neutral-300
      via-neutral-500
      to-black"
     >
      <div className="z-10 max-w-5xl w-full items-center justify-between font-mono text-sm lg:flex">
        <h1 className='text-lg text-white'>HELLO WORLD</h1>
      </div>

    </main>
    </Layout>

  )
}
