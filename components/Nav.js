import Link from 'next/link';

const Nav = () => {
  return (
    <nav className="
    flex flex-wrap
    items-center
    justify-between
    w-full
    py-4
    md:py-0
    px-4
    text-lg text-gray-700
    bg-white
    ">
      <div>
        <img 
        className="m-3 rounded-full shadow-custom"
        src='/123.png'
        width={100}
        alt='ahsoka logo'
        />
      </div>
      <div>
        <ul className="pt-4 text-base text-black md:flex md: justify-between md:pt-0 space-x-2 ">
          <li>
            <Link href='/'>
              <span className="md:p-2 py-2 block hover:text-blue-500">Home</span>
            </Link>
          </li>
          <li>
            <Link href='/films'>
              <span className="md:p-2 py-2 block hover:text-blue-500">Films</span>
            </Link>
          </li>
        </ul>
      </div>

    </nav>
  )
}

export default Nav;